<?php 
class Kegiatan_kt_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }

    public function get_kegiatan_id($id){
        $query = $this->db->get_where('kegiatan_kt', array('id'=>$id));
        return $query->row();
    }

    public function create_kegiatan(){
        //kegiatan_kt
        //acara jenis_kegiatan  deskripsi   tempat_pelaksanaan  waktu_pelaksanaan   dokumentasi
        return $this->db->insert('kegiatan_kt', [
            'acara' => $this->input->post('acara'),
            'jenis_kegiatan' => $this->input->post('jenis_kegiatan'), 
            'deskripsi' => $this->input->post('deskripsi'),
            'tempat_pelaksanaan' => $this->input->post('tempat_pelaksanaan'),
            'waktu_pelaksanaan' => $this->input->post('waktu_pelaksanaan'),
            'dokumentasi' => $this->input->post('dokumentasi')
        ]);
    }

    public function delete_kegiatan($id){
        $this->db->where('id', $id)->delete('kegiatan_kt');
        return true;
    }

    public function update_kegiatan(){
        $data = [
            'acara' => $this->input->post('acara'),
            'jenis_kegiatan' => $this->input->post('jenis_kegiatan'), 
            'deskripsi' => $this->input->post('deskripsi'),
            'tempat_pelaksanaan' => $this->input->post('tempat_pelaksanaan'),
            'waktu_pelaksanaan' => $this->input->post('waktu_pelaksanaan'),
            'dokumentasi' => $this->input->post('dokumentasi')
        ];
        return $this->db->where('id', $this->input->post('id'))->update('kegiatan_kt', $data);
    }

    public function get_kegiatan(){
       return $this->db->order_by('id')->get('kegiatan_kt')->result();
    }
}