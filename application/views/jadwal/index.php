<div class="container">
<?php $this->load->view('templates/ms'); ?>

<center><h2>Daftar Kegiatan</h2></center>
<br>
<a href="<?php echo base_url() ?>jadwal/tambah_kegiatan"><button class="btn btn-md btn-success">Tambah Kegiatan</button></a><br><br>
    <table id="example" class="table table-striped table-bordered" style="width: 100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal Kegiatan</th>
                    <th>Keterangan</th>
                    <th>Waktu Pelaksanaan</th>
                    <th>Tempat Pelaksanaan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; foreach ($kegiatans as  $kegiatan) :?>
                <tr>
                    <td><?= $no ?></td>
                    <td><?= $kegiatan->tanggal_kegiatan; ?></td>
                    <td><?= $kegiatan->keterangan ?></td>
                    <td><?= $kegiatan->waktu_pelaksanaan ?></td>
                    <td><?= $kegiatan->tempat_pelaksanaan ?></td>
                    <td style="text-align: center"><a href="<?php echo base_url() ?>jadwal/edit_kegiatan/<?= $kegiatan->id ?>"><button class="btn btn-xs btn-warning" style="font-size: 9px">Edit</button></a><a href="<?php echo base_url() ?>jadwal/hapus_kegiatan/<?= $kegiatan->id ?>"><button class="btn btn-xs btn-danger" style="font-size: 9px">Hapus</button></a></td>
                </tr>
                <?php $no++; endforeach; ?>
            </tbody>
    </table>


<center><h2>Daftar Booking Kegiatan</h2></center>
<br>
<a href="<?php echo base_url() ?>jadwal/tambah_booking"><button class="btn btn-md btn-success">Tambah Booking Kegiatan</button></a><br><br>
    <table id="example1" class="table table-striped table-bordered" style="width: 100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Lengkap</th>
                    <th>Alamat Acara</th>
                    <th>Nomor Telepon</th>
                    <th>Tanggal Acara</th>
                    <th>Jenis Kegiatan</th>
                    <th>Waktu Pelaksanaan</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; foreach ($bookings as  $booking) :?>
                <tr>
                    <td><?= $no ?></td>
                    <td><?= $booking->nama_lengkap ?></td>
                    <td><?= $booking->alamat_acara ?></td>
                    <td><?= $booking->nomor_telepon ?></td>
                    <td><?= $booking->tanggal_acara ?></td>
                    <td><?= $booking->jenis_kegiatan ?></td>
                    <td><?= $booking->waktu_pelaksanaan ?></td>
                    <td><?= $booking->keterangan ?></td>
                    <td style="text-align: center"><a href="<?php echo base_url() ?>jadwal/edit_booking/<?= $booking->id ?>"><button class="btn btn-xs btn-warning" style="font-size: 9px">Edit</button></a><a href="<?php echo base_url() ?>jadwal/hapus_booking/<?= $booking->id ?>"><button class="btn btn-xs btn-danger" style="font-size: 9px">Hapus</button></a></td>
                </tr>
                <?php $no++; endforeach; ?>
            </tbody>
    </table>
</div>
<br><br>