<style>
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #4ecdc4;
  color: white;
  text-align: center;
}
</style>

<div class="footer">
  <p style="font-size: 12px">Created With &#9825</p>
</div>


<script src="<?php echo base_url() ?>assets/js/jquery-3.5.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable();
    $('#example1').DataTable();
} );
</script>
</body>
</html>