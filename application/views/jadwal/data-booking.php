<div class="container">
    <div class="row justify-content-md-center">
        <center><h1>Data Booking Kegiatan</h1></center>
        
        <?php echo validation_errors(); ?>
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <a href="<?php echo base_url() ?>jadwal"><button class="btn btn-md btn-success">Lihat Jadwal</button></a><br><br>
            <?php echo (isset($bookings)) ? form_open('jadwal/update_booking') : form_open('jadwal/tambah_booking') ?>        
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap" value="<?php echo (isset($bookings)) ? $bookings->nama_lengkap : "" ;?>">
            </div>
            <div class="form-group">
                <label>Alamat Acara</label>
                <input type="text" class="form-control" name="alamat_acara" placeholder="Alamat Acara" value="<?php echo (isset($bookings)) ? $bookings->alamat_acara : "" ;?>">
            </div>
            <div class="form-group">
                <label>Nomor Telepon</label>
                <input type="text" class="form-control" name="nomor_telepon" placeholder="Nomor Telepon" value="<?php echo (isset($bookings)) ? $bookings->nomor_telepon : "" ;?>">
            </div>
            <div class="form-group">
                <label>Tanggal Acara</label>
                <input type="text" class="form-control" name="tanggal_acara" placeholder="Tanggal Acara" value="<?php echo (isset($bookings)) ? $bookings->tanggal_acara : "" ;?>">
            </div>
            <div class="form-group">
                <label>Jenis Kegiatan</label>
                <input type="text" class="form-control" name="jenis_kegiatan" placeholder="Jenis Kegiatan" value="<?php echo (isset($bookings)) ? $bookings->jenis_kegiatan : "" ;?>">
            </div>
            <div class="form-group">
                <label>Waktu Pelaksanaan</label>
                <input type="text" class="form-control" name="waktu_pelaksanaan" placeholder="Waktu Pelaksanaan" value="<?php echo (isset($bookings)) ? $bookings->waktu_pelaksanaan : "" ;?>">
            </div>
            <div class="form-group">
                <label>Keterangan</label>
                <input type="text" class="form-control" name="keterangan" placeholder="Keterangan" value="<?php echo (isset($bookings)) ? $bookings->keterangan : "" ;?>">
            </div>
            <input type="hidden" class="form-control" name="id" value="<?php echo (isset($bookings)) ? $bookings->id : "" ;?>">
            <input type="submit" name="submit" value="Update/Tambah Booking Kegiatan" class="btn btn-primary btn-block">
        <?php echo form_close(); ?>
        </div>
        <div class="col-md-2">
        </div>
        
    </div>
</div>
