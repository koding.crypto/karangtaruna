<div class="container">
<?php $this->load->view('templates/ms'); ?>
<center><h2><?=$title?></h2></center>
<br>
<a href="<?php echo base_url() ?>anggota/tambah"><button class="btn btn-md btn-success">Tambah Anggota</button></a><br><br>
    <table id="example" class="table table-striped table-bordered" style="width: 100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama Lengkap</th>
                    <th>No KTP</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                    <th>Alamat</th>
                    <th>Nomor Telepon</th>
                    <th>Email</th>
                    <th>Agama</th>
                    <th>Pekerjaan</th>
                    <th>Jenis Kelamin</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; foreach ($posts as  $post) :?>
                <tr>
                    <td><?= $no ?></td>
                    <td><?= $post->nama_lengkap ?></td>
                    <td><?= $post->no_ktp ?></td>
                    <td><?= $post->tempat_lahir ?></td>
                    <td><?= $post->tanggal_lahir ?></td>
                    <td><?= $post->alamat ?></td>
                    <td><?= $post->nomor_telepon ?></td>
                    <td><?= $post->email ?></td>
                    <td><?= $post->agama ?></td>
                    <td><?= $post->pekerjaan ?></td>
                    <td><?= $post->jenis_kelamin ?></td>
                    <td><?= $post->keterangan ?></td>
                    <td style="text-align: center"><a href="<?php echo base_url() ?>anggota/edit/<?= $post->id ?>"><button class="btn btn-xs btn-warning" style="font-size: 9px">Edit</button></a><a href="<?php echo base_url() ?>anggota/delete/<?= $post->id ?>"><button class="btn btn-xs btn-danger" style="font-size: 9px">Hapus</button></a></td>
                </tr>
                <?php $no++; endforeach; ?>
            </tbody>
    </table>

</div>
<br><br>
