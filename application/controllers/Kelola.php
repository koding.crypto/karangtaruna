<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelola extends CI_Controller {

    public function index(){
        $data['title'] = 'Lihat Anggota';
        $data['kegiatans'] = $this->kegiatan_kt_model->get_kegiatan();
        $data['pendaftars'] = $this->pendaftar_kt_model->get_pendaftar();
        $this->load->view('templates/header');
        $this->load->view('kelola/index', $data);
        $this->load->view('templates/footer');
    }

    /*KEGIATAN KARANG TARUNA*/

    public function tambah_kegiatan_kt(){
        $this->check_login();
        $data['title'] = 'Formulir Data Anggota';
        if(!$this->input->post('submit')){
            $this->load->view('templates/header');
            $this->load->view('kelola/data-kt', $data);
            $this->load->view('templates/footer');
        } else {
            $this->kegiatan_kt_model->create_kegiatan();
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
            redirect('kelola');
        }
    }
    
    public function hapus_kegiatan($id){
        // check login
        $this->check_login();
        $this->kegiatan_kt_model->delete_kegiatan($id);
        $this->session->set_flashdata('danger', 'Berhasil Menghapus Data');
        redirect('kelola');
    }

    public function edit_kegiatan($id){
        $this->check_login();
        $data['kegiatans'] = $this->kegiatan_kt_model->get_kegiatan_id($id);
        $this->load->view('templates/header');
        $this->load->view('kelola/data-kt', $data);
        $this->load->view('templates/footer');
    }

    public function update_kegiatan(){
        $this->kegiatan_kt_model->update_kegiatan();
        $this->session->set_flashdata('success', 'Berhasil Mengupdate Kegiatan');
        redirect('kelola');
    }

    /*END KEGIATAN KT*/


    public function tambah_anggota_kt(){
        $this->check_login();
        $data['title'] = 'Formulir Data Anggota';
        if(!$this->input->post('submit')){
            $this->load->view('templates/header');
            $this->load->view('kelola/data-anggota', $data);
            $this->load->view('templates/footer');
        } else {
            $this->pendaftar_kt_model->create_pendaftar();
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
            redirect('kelola');
        }
    }
    
    public function hapus_anggota($id){
        // check login
        $this->check_login();
        $this->pendaftar_kt_model->delete_pendaftar($id);
        $this->session->set_flashdata('danger', 'Berhasil Menghapus Data');
        redirect('kelola');
    }

    public function edit_anggota($id){
        $this->check_login();
        $data['pendaftar'] = $this->pendaftar_kt_model->get_pendaftar_id($id);
        $this->load->view('templates/header');
        $this->load->view('kelola/data-anggota', $data);
        $this->load->view('templates/footer');
    }

    public function update_anggota(){
        $this->pendaftar_kt_model->update_pendaftar();
        $this->session->set_flashdata('success', 'Berhasil Mengupdate Kegiatan');
        redirect('kelola');
    }

    private function check_login(){
        // check login
        if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
    }
}
