<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {

    public function index(){
        $this->check_login();
        $data['title'] = 'Lihat Jadwal Kegiatan';
        $data['kegiatans'] = $this->kegiatan_model->get_kegiatan();
        $data['bookings'] = $this->booking_model->get_booking();
        $this->load->view('templates/header');
        $this->load->view('jadwal/index', $data);
        $this->load->view('templates/footer');
    }


    public function user_index(){
        $data['title'] = 'Lihat Jadwal Kegiatan';
        $data['kegiatans'] = $this->kegiatan_model->get_kegiatan();
        $this->load->view('templates/header');
        $this->load->view('jadwal/user_index', $data);
        $this->load->view('templates/footer');
    }

    /*UNTUK TAMBAH KEGIATAN*/
    public function tambah_kegiatan(){
        $this->check_login();
        $data['title'] = 'Formulir Data Kegiatan';
        if(!$this->input->post('submit')){
            $this->load->view('templates/header');
            $this->load->view('jadwal/data-kegiatan', $data);;
            $this->load->view('templates/footer');
        } else {
            $this->kegiatan_model->create_kegiatan();
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Kegiatan');
            redirect('jadwal');
        }
    }

    public function hapus_kegiatan($id){
        // check login
        $this->check_login();
        $this->kegiatan_model->delete_kegiatan($id);
        $this->session->set_flashdata('danger', 'Berhasil Menghapus Kegiatan');
        redirect('jadwal');
    }

    public function edit_kegiatan($id){
        $this->check_login();
        $data['kegiatans'] = $this->kegiatan_model->get_kegiatan_id($id);
        $data['title'] = 'Formulir Data Kegiatan';
        $this->load->view('templates/header');
        $this->load->view('jadwal/data-kegiatan', $data);;
        $this->load->view('templates/footer');

    }

    public function update_kegiatan(){
        $this->kegiatan_model->update_kegiatan();
        $this->session->set_flashdata('success', 'Berhasil Mengupdate Kegiatan');
        redirect('jadwal');
    }

    /*UNTUK BOOKING KEGIATAN*/
    
    public function tambah_booking(){
        $this->check_login();
        $data['title'] = 'Formulir Data Kegiatan';
        if(!$this->input->post('submit')){
            $this->load->view('templates/header');
            $this->load->view('jadwal/data-booking', $data);;
            $this->load->view('templates/footer');
        } else {
            $this->booking_model->create_booking();
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Booking Kegiatan');
            redirect('jadwal');
        }
    }

    public function hapus_booking($id){
        // check login
        $this->check_login();
        $this->booking_model->delete_booking($id);
        $this->session->set_flashdata('danger', 'Berhasil Menghapus Booking');
        redirect('jadwal');
    }

    public function edit_booking($id){
        $this->check_login();
        $data['bookings'] = $this->booking_model->get_booking_id($id);
        $data['title'] = 'Formulir Data Kegiatan';
        $this->load->view('templates/header');
        $this->load->view('jadwal/data-booking', $data);;
        $this->load->view('templates/footer');
    }
    public function update_booking(){
        $this->booking_model->update_booking();
        $this->session->set_flashdata('success', 'Berhasil Mengupdate Kegiatan');
        redirect('jadwal');
    }

    /*END BOOKING*/

    private function check_login(){
        // check login
        if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
    }
}
