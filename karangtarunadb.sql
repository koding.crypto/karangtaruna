-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 04, 2020 at 11:26 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karangtarunadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

DROP TABLE IF EXISTS `anggota`;
CREATE TABLE IF NOT EXISTS `anggota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(56) NOT NULL,
  `no_ktp` varchar(56) NOT NULL,
  `tempat_lahir` varchar(56) NOT NULL,
  `tanggal_lahir` varchar(56) NOT NULL,
  `alamat` text NOT NULL,
  `nomor_telepon` varchar(15) NOT NULL,
  `email` varchar(56) NOT NULL,
  `agama` varchar(32) NOT NULL,
  `pekerjaan` varchar(56) NOT NULL,
  `jenis_kelamin` varchar(16) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id`, `nama_lengkap`, `no_ktp`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `nomor_telepon`, `email`, `agama`, `pekerjaan`, `jenis_kelamin`, `keterangan`) VALUES
(2, 'Leonardional', '7471100505951001', 'Sumatera Barat', '1 Juli 1997', 'Sawangan Permai Indah', '081299205208', 'leonardional@gmail.com', 'Islam', 'Engineer', 'laki-laki', '-');

-- --------------------------------------------------------

--
-- Table structure for table `booking_kegiatan`
--

DROP TABLE IF EXISTS `booking_kegiatan`;
CREATE TABLE IF NOT EXISTS `booking_kegiatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(56) NOT NULL,
  `alamat_acara` text NOT NULL,
  `nomor_telepon` varchar(16) NOT NULL,
  `tanggal_acara` varchar(56) NOT NULL,
  `jenis_kegiatan` varchar(56) NOT NULL,
  `waktu_pelaksanaan` varchar(56) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_kegiatan`
--

INSERT INTO `booking_kegiatan` (`id`, `nama_lengkap`, `alamat_acara`, `nomor_telepon`, `tanggal_acara`, `jenis_kegiatan`, `waktu_pelaksanaan`, `keterangan`) VALUES
(2, 'nama', 'alamat', 'nomor', 'tanggal', 'jenis', 'waktu', 'keterangan');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `user_id`, `name`, `created_at`) VALUES
(3, 1, 'Informasi', '2020-06-04 08:25:09'),
(4, 1, 'Kegiatan', '2020-06-04 08:25:16');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `name`, `email`, `body`, `created_at`) VALUES
(1, 3, 'haisultra', 'leonardional@gmail.com', 'percobaan komen', '2020-06-02 13:31:02');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

DROP TABLE IF EXISTS `kegiatan`;
CREATE TABLE IF NOT EXISTS `kegiatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_kegiatan` varchar(32) NOT NULL,
  `keterangan` text NOT NULL,
  `waktu_pelaksanaan` varchar(56) NOT NULL,
  `tempat_pelaksanaan` varchar(102) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id`, `tanggal_kegiatan`, `keterangan`, `waktu_pelaksanaan`, `tempat_pelaksanaan`) VALUES
(2, '1 Juni 2020', 'Hari Lahir Pancasila', '08.00-09.00', 'Aula Gedung Serbaguna');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan_kt`
--

DROP TABLE IF EXISTS `kegiatan_kt`;
CREATE TABLE IF NOT EXISTS `kegiatan_kt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acara` varchar(255) NOT NULL,
  `jenis_kegiatan` varchar(56) NOT NULL,
  `deskripsi` text NOT NULL,
  `tempat_pelaksanaan` varchar(56) NOT NULL,
  `waktu_pelaksanaan` varchar(32) NOT NULL,
  `dokumentasi` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kegiatan_kt`
--

INSERT INTO `kegiatan_kt` (`id`, `acara`, `jenis_kegiatan`, `deskripsi`, `tempat_pelaksanaan`, `waktu_pelaksanaan`, `dokumentasi`) VALUES
(2, 'acara', 'jenis', 'deskripsi', 'tempat', 'waktu', 'dokumentasi');

-- --------------------------------------------------------

--
-- Table structure for table `pendaftar_kt`
--

DROP TABLE IF EXISTS `pendaftar_kt`;
CREATE TABLE IF NOT EXISTS `pendaftar_kt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(56) NOT NULL,
  `alamat` text NOT NULL,
  `nomor_telepon` varchar(16) NOT NULL,
  `email` varchar(56) NOT NULL,
  `jenis_kelamin` varchar(16) NOT NULL,
  `pekerjaan` varchar(56) NOT NULL,
  `kegiatan` varchar(102) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendaftar_kt`
--

INSERT INTO `pendaftar_kt` (`id`, `nama_lengkap`, `alamat`, `nomor_telepon`, `email`, `jenis_kelamin`, `pekerjaan`, `kegiatan`) VALUES
(2, 'percobaanx', 'alamat', 'nomor', 'email', 'laki-laki', '1', 'apa');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `post_image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `user_id`, `title`, `slug`, `body`, `post_image`, `created_at`) VALUES
(10, 3, 1, 'Pengertian Reog Ponorogo', 'Pengertian-Reog-Ponorogo', '<p>Reog adalah salah satu kesenian budaya yang berasal dari Jawa Timur bagian barat-laut dan Ponorogo dianggap sebagai kota asal Reog yang sebenarnya. Gerbang kota Ponorogo dihiasi oleh sosok Warok dan Gemblak, dua sosok yang ikut tampil pada saat Reog dipertunjukkan. Reog adalah salah satu bukti budaya daerah di Indonesia yang masih sangat kental dengan hal-hal yang berbau mistik dan ilmu kebatinan yang kuat.<br><br>Pada dasarnya ada lima versi cerita populer yang berkembang di masyarakat tentang asal-usul Reog dan Warok, namun salah satu cerita yang paling terkenal adalah cerita tentang pemberontakan Ki Ageng Kutu, seorang abdi kerajaan pada masa Bra Kertabumi, Raja Majapahit terakhir yang berkuasa pada abad ke-15. Ki Ageng Kutu murka akan pengaruh kuat dari pihak rekan Cina rajanya dalam pemerintahan dan prilaku raja yang korup, ia pun melihat bahwa kekuasaan Kerajaan Majapahit akan berakhir.<br><br>Ia lalu meninggalkan sang raja dan mendirikan perguruan dimana ia mengajar anak-anak muda seni bela diri, ilmu kekebalan diri, dan ilmu kesempurnaan dengan harapan bahwa anak-anak muda ini akan menjadi bibit dari kebangkitan lagi kerajaan Majapahit kelak. Sadar bahwa pasukannya terlalu kecil untuk melawan pasukan kerajaan maka pesan politis Ki Ageng Kutu disampaikan melalui pertunjukan seni Reog, yang merupakan \"sindiran\" kepada Raja Bra Kertabumi dan kerajaannya. Pagelaran Reog menjadi cara Ki Ageng Kutu membangun perlawanan masyarakat lokal menggunakan kepopuleran Reog.<br><br>Dalam pertunjukan Reog ditampilkan topeng berbentuk kepala singa yang dikenal sebagai \"Singa Barong\", raja hutan, yang menjadi simbol untuk Kertabumi, dan diatasnya ditancapkan bulu-bulu merak hingga menyerupai kipas raksasa yang menyimbolkan pengaruh kuat para rekan Cinanya yang mengatur dari atas segala gerak-geriknya.<br><br>Jatilan, yang diperankan oleh kelompok penari gemblak yang menunggangi kuda-kudaan menjadi simbol kekuatan pasukan Kerajaan Majapahit yang menjadi perbandingan kontras dengan kekuatan warok, yang berada dibalik topeng badut merah yang menjadi simbol untuk Ki Ageng Kutu, sendirian dan menopang berat topeng singabarong yang mencapai lebih dari 50kg hanya dengan menggunakan giginya.<br><br>Populernya Reog Ki Ageng Kutu akhirnya menyebabkan Kertabumi mengambil tindakan dan menyerang perguruannya, pemberontakan oleh warok dengan cepat diatasi, dan perguruan dilarang untuk melanjutkan pengajaran akan warok. Namun murid-murid Ki Ageng kutu tetap melanjutkannya secara diam-diam. Walaupun begitu, kesenian Reognya sendiri masih diperbolehkan untuk dipentaskan karena sudah menjadi pertunjukan populer diantara masyarakat, namun jalan ceritanya memiliki alur baru dimana ditambahkan karakter-karakter dari cerita rakyat Ponorogo yaitu Kelono Sewondono, Dewi Songgolangit, and Sri Genthayu.<br><br>Versi resmi alur cerita Reog Ponorogo kini adalah cerita tentang Raja Ponorogo yang berniat melamar putri Kediri, Dewi Ragil Kuning, namun ditengah perjalanan ia dicegat oleh Raja Singabarong dari Kediri. Pasukan Raja Singabarong terdiri dari merak dan singa, sedangkan dari pihak Kerajaan Ponorogo Raja Kelono dan Wakilnya Bujanganom, dikawal oleh warok (pria berpakaian hitam-hitam dalam tariannya), dan warok ini memiliki ilmu hitam mematikan. Seluruh tariannya merupakan tarian perang antara Kerajaan Kediri dan Kerajaan Ponorogo, dan mengadu ilmu hitam antara keduanya, para penari dalam keadaan \'kerasukan\' saat mementaskan tariannya.<br><br>Hingga kini masyarakat Ponorogo hanya mengikuti apa yang menjadi warisan leluhur mereka sebagai pewarisan budaya yang sangat kaya. Dalam pengalamannya Seni Reog merupakan cipta kreasi manusia yang terbentuk adanya aliran kepercayaan yang ada secara turun temurun dan terjaga. Upacaranya pun menggunakan syarat-syarat yang tidak mudah bagi orang awam untuk memenuhinya tanpa adanya garis keturunan yang jelas. mereka menganut garis keturunan Parental dan hukum adat yang masih berlaku.</p>', 'regos.jpg', '2020-06-04 23:17:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `zipcode`, `email`, `username`, `password`, `register_date`) VALUES
(1, 'moderator', '93317', 'moderator@web.com', 'moderator', '$2y$12$w9e7guLBfFI2S9F.B2uJ3O9obWOcmUBxu/Quujwy/nYZBCnObXAnG', '2020-06-02 03:58:29');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
