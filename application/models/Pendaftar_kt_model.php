<?php 
class Pendaftar_kt_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }

    public function get_pendaftar_id($id){
        $query = $this->db->get_where('pendaftar_kt', array('id'=>$id));
        return $query->row();
    }

    public function create_pendaftar(){
        //pendaftar_kt
        //nama_lengkap  alamat  nomor_telepon   email   jenis_kelamin   pekerjaan   kegiatan
        return $this->db->insert('pendaftar_kt', [
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'alamat' => $this->input->post('alamat'), 
            'nomor_telepon' => $this->input->post('nomor_telepon'),
            'email' => $this->input->post('email'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'pekerjaan' => $this->input->post('pekerjaan'),
            'kegiatan' => $this->input->post('kegiatan')
        ]);
    }

    public function delete_pendaftar($id){
        $this->db->where('id', $id)->delete('pendaftar_kt');
        return true;
    }

    public function update_pendaftar(){
        $data = [
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'alamat' => $this->input->post('alamat'), 
            'nomor_telepon' => $this->input->post('nomor_telepon'),
            'email' => $this->input->post('email'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'pekerjaan' => $this->input->post('pekerjaan'),
            'kegiatan' => $this->input->post('kegiatan')
        ];
        return $this->db->where('id', $this->input->post('id'))->update('pendaftar_kt', $data);
    }

    public function get_pendaftar(){
       return $this->db->order_by('id')->get('pendaftar_kt')->result();
    }
}