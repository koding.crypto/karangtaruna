<div class="container">
<?php $this->load->view('templates/ms'); ?>

<center><h2>Daftar Kegiatan</h2></center>
<br>
    <table id="example" class="table table-striped table-bordered" style="width: 100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal Kegiatan</th>
                    <th>Keterangan</th>
                    <th>Waktu Pelaksanaan</th>
                    <th>Tempat Pelaksanaan</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; foreach ($kegiatans as  $kegiatan) :?>
                <tr>
                    <td><?= $no ?></td>
                    <td><?= $kegiatan->tanggal_kegiatan; ?></td>
                    <td><?= $kegiatan->keterangan ?></td>
                    <td><?= $kegiatan->waktu_pelaksanaan ?></td>
                    <td><?= $kegiatan->tempat_pelaksanaan ?></td>
                </tr>
                <?php $no++; endforeach; ?>
            </tbody>
    </table>
</div>
<br><br>