<div class="container">
    <div class="row justify-content-md-center">
        <center><h1>Data Kegiatan</h1></center>
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <a href="<?php echo base_url() ?>jadwal"><button class="btn btn-md btn-success">Lihat Jadwal</button></a><br><br>
            <?php echo validation_errors(); ?>
            <?php echo (isset($kegiatans)) ? form_open('jadwal/update_kegiatan') : form_open('jadwal/tambah_kegiatan') ?>    
            <div class="form-group">
                <label>Tanggal Kegiatan</label>
                <input type="text" class="form-control" name="tanggal_kegiatan" placeholder="Tanggal Kegiatan" value="<?php echo (isset($kegiatans)) ? $kegiatans->tanggal_kegiatan : "" ;?>">
            </div>
            <div class="form-group">
                <label>Keterangan</label>
                <input type="text" class="form-control" name="keterangan" placeholder="keterangan" value="<?php echo (isset($kegiatans)) ? $kegiatans->keterangan : "" ;?>">
            </div>
            <div class="form-group">
                <label>Waktu Pelaksanaan</label>
                <input type="text" class="form-control" name="waktu_pelaksanaan" placeholder="Waktu Pelaksanaan" value="<?php echo (isset($kegiatans)) ? $kegiatans->waktu_pelaksanaan : "" ;?>">
            </div>
            <div class="form-group">
                <label>Tempat Pelaksanaan</label>
                <input type="text" class="form-control" name="tempat_pelaksanaan" placeholder="Tempat Pelaksanaan" value="<?php echo (isset($kegiatans)) ? $kegiatans->tempat_pelaksanaan : "" ;?>">
            </div>
            <input type="hidden" class="form-control" name="id" value="<?php echo (isset($kegiatans)) ? $kegiatans->id : "" ;?>">
            <input type="submit" value="Tambah/Update Data Kegiatan" name="submit" class="btn btn-primary btn-block">
        <?php echo form_close(); ?>
        </div>
        <div class="col-md-2">
        </div>
        
    </div>
</div>
